<?php

namespace DreamCat\AnnotationParser\AnnotationFuncParser;

use PHPUnit\Framework\TestCase;

/**
 * ReturnParamParser的测试类
 * @author vijay
 */
class ReturnParserTest extends TestCase
{
    /**
     * 测试getAttention方法
     * @return ReturnParser 供下一测试方法用的对象
     */
    public function testGetAttention()
    {
        $obj = new ReturnParser();
        static::assertEquals(["return"], $obj->getAttention());
        return $obj;
    }

    /**
     * 测试parse方法
     * @param string $returnDesc 返回值注解文档
     * @param array $returnParseReturn 预期的解析结果
     * @param ReturnParser $parser 解析器
     * @return void
     * @dataProvider parseTestData
     * @depends      testGetAttention
     */
    public function testParse(string $returnDesc, array $returnParseReturn, ReturnParser $parser)
    {
        static::assertEquals($returnParseReturn, $parser->parse("return", $returnDesc), "[{$returnDesc}]解析出错");
    }

    /**
     * parse测试方法的数据供给器
     * @return array 数据供给器，每个元素是一条测试用例，元素结构{
     *      第一个值是“返回值注解文档”
     *      第二个值是“预期的解析结果”
     * }
     */
    public function parseTestData()
    {
        return [
            ["void", ["type" => "void", "desc" => ""]],
            ["忘记写类型的", ["type" => ReturnParser::TYPE_UNKNOWN, "desc" => "忘记写类型的"]],
            [" array 解析后的结果", ["type" => "array", "desc" => "解析后的结果"]],
            ["mixed 解析后的结果", ["type" => "mixed", "desc" => "解析后的结果"]],
        ];
    }
}

# end of file
