<?php

namespace DreamCat\AnnotationParser\AnnotationFuncParser;

use PHPUnit\Framework\TestCase;

/**
 * ParamParser测试类
 * @author vijay
 */
class ParamParserTest extends TestCase
{
    /**
     * 测试getAttention方法
     * @return ParamParser 供下一测试方法用的对象
     */
    public function testGetAttention()
    {
        $obj = new ParamParser();
        static::assertEquals(["param"], $obj->getAttention());
        return $obj;
    }

    /**
     * 测试parse方法
     * @param string $paramDesc 返回值注解文档
     * @param array $paramParseReturn 预期的解析结果
     * @param ParamParser $parser 解析器
     * @return void
     * @dataProvider parseTestData
     * @depends      testGetAttention
     */
    public function testParse(string $paramDesc, array $paramParseReturn, ParamParser $parser)
    {
        static::assertEquals($paramParseReturn, $parser->parse("param", $paramDesc), "[{$paramDesc}]解析出错");
    }

    /**
     * parse测试方法的数据供给器
     * @return array 数据供给器，每个元素是一条测试用例，元素结构{
     *      第一个值是“参数注解文档”
     *      第二个值是“预期的解析结果”
     * }
     */
    public function parseTestData()
    {
        return [
            ["\$var 忘记写类型", ["var" => "var", "type" => ParamParser::TYPE_UNKNOWN, "desc" => "忘记写类型"]],
            ["忘记写类型和参数的", ["var" => "", "type" => ParamParser::TYPE_UNKNOWN, "desc" => "忘记写类型和参数的"]],
            [" array \$var 解析参数", ["var" => "var", "type" => "array", "desc" => "解析参数"]],
            [" array \$var", ["var" => "var", "type" => "array", "desc" => ""]],
        ];
    }
}

# end of file
