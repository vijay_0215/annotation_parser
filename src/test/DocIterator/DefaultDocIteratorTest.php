<?php

namespace DreamCat\AnnotationParser\DocIterator;

use PHPUnit\Framework\TestCase;

/**
 * 默认的注解文档拆解的分隔器的测试类
 * @author vijay
 */
class DefaultDocIteratorTest extends TestCase
{
    /**
     * testIterator
     * 测试注解拆解
     * @param string $docComents 注解文档
     * @param array $expectedRet 预期结果
     * @return void
     * @dataProvider docComments
     */
    public function testIterator(string $docComents, array $expectedRet)
    {
        $it = new DefaultDocIterator();
        $it->setDocComment($docComents);
        $parseRet = [];
        foreach ($it as $key => $val) {
            $parseRet[] = [$key, $val];
        }
        self::assertEquals($expectedRet, $parseRet);
    }

    /**
     * docComments
     * 测试注解拆解的测试用例
     * @return array 每个元素是一组测试用例，第一个参数是注解文档，第二个参数是预期解析的结果数组
     */
    public function docComments()
    {
        return [
            [
                "    /**
     * setDocComment
     * 设定注解文档
     * 
     * @param string \$docComment 注解文档
     * @return IDocCommentIterator 当前对象自身
     */",
                [
                    ["", "setDocComment",],
                    ["", "设定注解文档",],
                    ["param", "string \$docComment 注解文档",],
                    ["return", "IDocCommentIterator 当前对象自身",],
                ],
            ],
            [
                "/** @var \DreamCat\Array2Class\Demo\Inner\Status */",
                [
                    ["var", "\DreamCat\Array2Class\Demo\Inner\Status"]
                ],
            ],
        ];
    }
}

# end of file
