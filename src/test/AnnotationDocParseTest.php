<?php

namespace DreamCat\AnnotationParser;

use PHPUnit\Framework\TestCase;

/**
 * 注解文档解析器测试用例
 * @author vijay
 */
class AnnotationDocParseTest extends TestCase
{
    /**
     * 测试文档解析主流程
     * @return void
     */
    public function testParse()
    {
        $docComment = "@param kk\n@return mm\n@notreg";
        $exceptRet = ["param" => ["kk"], "return" => "mm"];
        [$paramParseMock, $returnParseMock] = $this->createAnnotationMock();
        $parser = new AnnotationDocParse();
        /** @noinspection PhpParamsInspection */
        $parser
            ->addAnnotationParse($paramParseMock)
            ->addAnnotationParse($returnParseMock)
            ->setDocCommentIterator($this->createItMock($docComment))
            ->setOutputFormater($this->createFormaterMock($exceptRet, $paramParseMock, $returnParseMock));
        $ret = $parser->parse($docComment);
        static::assertEquals($exceptRet, $ret);
    }

    /**
     * 创建注解文档拆解的分隔器mock
     * @param string $docComment 文档
     * @return \PHPUnit\Framework\MockObject\MockObject mock对象
     */
    private function createItMock(string $docComment)
    {
        $itMock = $this->getMockForAbstractClass(IDocCommentIterator::class);
        # 表示setDocComment会被调用一次，参数为$docComment
        $itMock->expects(static::once())->method("setDocComment")->with(static::equalTo($docComment));
        # 表示rewind方法会被调用一次
        $itMock->expects(static::once())->method("rewind");
        # 表示方法valid会被调用4次，依次返回true、true、true、false
        $itMock
            ->expects(static::exactly(4))
            ->method("valid")
            ->willReturnOnConsecutiveCalls(true, true, true, false);
        $itMock
            ->expects(static::exactly(3))
            ->method("next");
        $itMock
            ->expects(static::exactly(3))
            ->method("key")
            ->willReturnOnConsecutiveCalls("param", "return", "notreg");
        $itMock
            ->expects(static::exactly(3))
            ->method("current")
            ->willReturnOnConsecutiveCalls("kk", "mm", "");
        return $itMock;
    }

    /**
     * 创建解析结果格式化接口mock
     * @param array $exceptRet 预期的输出结果
     * @param \PHPUnit\Framework\MockObject\MockObject $paramParse =
     * @param  \PHPUnit\Framework\MockObject\MockObject $returnParse =
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    private function createFormaterMock(array $exceptRet, $paramParse, $returnParse)
    {
        $mock = $this->getMockForAbstractClass(IDocParseResultFormater::class);
        $mock->expects(static::once())->method("reset");
        $mock->expects(static::once())->method("getResult")->willReturn($exceptRet);
        $mock
            ->expects(static::exactly(2))
            ->method("addResult")
            ->withConsecutive(
                [static::equalTo($paramParse), static::equalTo("param"), static::equalTo("kk")],
                [static::equalTo($returnParse), static::equalTo("return"), static::equalTo("mm")]
            );
        return $mock;
    }

    /**
     * 创建函数解析器mock
     * @return \PHPUnit\Framework\MockObject\MockObject[] 前者是param解析器，后者是return解析器
     */
    private function createAnnotationMock()
    {
        # 创建参数解析器mock
        $paramMock = $this->getMockForAbstractClass(IAnnotationParse::class);
        $paramMock->expects(static::once())->method("getAttention")->willReturn(["param", "spec"]);
        $paramMock
            ->expects(static::once())
            ->method("parse")
            ->with(static::equalTo("param"), static::equalTo("kk"))
            ->willReturn("kk");

        # 创建返回值解析器mock
        $returnMock = $this->getMockForAbstractClass(IAnnotationParse::class);
        $returnMock->expects(static::once())->method("getAttention")->willReturn(["return", "spec"]);
        $returnMock
            ->expects(static::once())
            ->method("parse")
            ->with(static::equalTo("return"), static::equalTo("mm"))
            ->willReturn("mm");
        return [$paramMock, $returnMock];
    }
}

# end of file
