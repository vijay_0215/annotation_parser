<?php

namespace DreamCat\AnnotationParser\OutputFormater;

use PHPUnit\Framework\TestCase;
use DreamCat\AnnotationParser\IAnnotationParse;

/**
 * FuncParseFormater测试类
 * @author vijay
 */
class FuncParseFormaterTest extends TestCase
{
    /**
     * 测试format流程
     * @param array $docs 测试用的解析后结果，每个元素的第一个值是对应的注解函数，第二个值是解析结果
     * @param array $expect 预期的组合后结果
     * @return void
     * @dataProvider formatData
     */
    public function testFormat(array $docs, array $expect)
    {
        $formater = new FuncParseFormater();
        $formater->reset();
        $mock = static::getMockForAbstractClass(IAnnotationParse::class);
        foreach ($docs as $doc) {
            /** @var IAnnotationParse $mock */
            $formater->addResult($mock, $doc[0], $doc[1]);
        }
        static::assertEquals($expect, $formater->getResult());
    }

    /**
     * 测试format流程的测试数据
     * @return array
     */
    public function formatData()
    {
        return [
            [
                [
                    ["", "testFormat"],
                    ["", "测试format流程"],
                    ["param", ["type" => "array", "var" => "docs", "desc" => "测试用的解析后结果"]],
                    ["param", ["type" => "array", "var" => "expect", "desc" => "预期的组合后结果"]],
                    ["return", ["type" => "void", "desc" => ""]],
                ],
                [
                    "fnDesc" => [
                        "testFormat",
                        "测试format流程",
                    ],
                    "params" => [
                        "docs" => [
                            "type" => "array",
                            "var" => "docs",
                            "desc" => "测试用的解析后结果",
                        ],
                        "expect" => [
                            "type" => "array",
                            "var" => "expect",
                            "desc" => "预期的组合后结果",
                        ],
                    ],
                    "return" => [
                        "type" => "void",
                        "desc" => "",
                    ],
                ],
            ],
        ];
    }
}

# end of file
