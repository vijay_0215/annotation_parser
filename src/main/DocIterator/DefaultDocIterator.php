<?php

namespace DreamCat\AnnotationParser\DocIterator;

use DreamCat\AnnotationParser\IDocCommentIterator;

/**
 * 默认的注解文档拆解的分隔器
 * @author vijay
 */
class DefaultDocIterator implements IDocCommentIterator
{
    /** @var array 用户换行符分隔的注解文档 */
    private $docs = [];
    /** @var int 当前迭代到的行数 */
    private $point = 0;

    /**
     * 设定注解文档
     * @param string $docComment 注解文档
     * @return IDocCommentIterator 当前对象自身
     */
    public function setDocComment(string $docComment): IDocCommentIterator
    {
        $this->docs = $this->splitDoc($docComment);
        $this->point = 0;
        return $this;
    }

    /**
     * 分割注解文档
     * @param string $docComment 注解文档
     * @return array
     */
    private function splitDoc(string $docComment): array
    {
        $list = [];
        $docs = explode("\n", $docComment);
        $len = count($docs);
        $current = [];
        for ($idx = 0; $idx < $len; ++$idx) {
            # 去除前后空格，去除注释前后的包围符
            $doc = trim(trim(trim(trim($docs[$idx]), "/"), "*"));
            if (!strlen($doc)) {
                continue;
            }
            if ($doc[0] == "@") {
                if ($current) {
                    $list[] = $current;
                }
                $tmp = explode(" ", $doc, 2);
                $current = ["key" => substr($tmp[0], 1), "current" => []];
                $doc = $tmp[1] ?? "";
            }
            if (strlen($doc)) {
                if ($current) {
                    $current["current"][] = $doc;
                } else {
                    $list[] = ["key" => "", "current" => [$doc]];
                }
            }
        }
        if ($current) {
            $list[] = $current;
        }
        return $list;
    }

    /**
     * current
     * 返回当前注解函数的对应注解内容
     * @return string 当前注解函数的对应注解内容
     */
    public function current(): string
    {
        return implode("\n", $this->docs[$this->point]["current"]);
    }

    /**
     * key
     * 返回当前注解函数
     * @return string 当前注解函数
     */
    public function key(): string
    {
        return $this->docs[$this->point]["key"];
    }

    /**
     * rewind
     * 返回到迭代器的第一个元素
     * @return void
     */
    public function rewind(): void
    {
        $this->point = 0;
    }

    /**
     * next
     * 向前移动到下一个元素
     * @return void
     */
    public function next(): void
    {
        ++$this->point;
    }

    /**
     * valid
     * 检查当前位置是否有效
     * @return bool 当前位置是否有效
     */
    public function valid(): bool
    {
        return $this->point < count($this->docs);
    }
}

# end of file
