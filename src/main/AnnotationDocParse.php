<?php

namespace DreamCat\AnnotationParser;

/**
 * 注解文档解析器
 * @author vijay
 */
class AnnotationDocParse
{
    /** @var IAnnotationParse[][] 所有的特定文档函数解析器 */
    private $annotations = [];
    /** @var IDocCommentIterator 注解文档拆解的分隔器 */
    private $docIterator;
    /** @var IDocParseResultFormater 输出结果格式器 */
    private $outputFormater;

    /**
     * 添加特定文档函数的解析器
     * @param IAnnotationParse $parser 特定文档函数的解析器
     * @return AnnotationDocParse 当前对象本身
     */
    public function addAnnotationParse(IAnnotationParse $parser): AnnotationDocParse
    {
        $keys = $parser->getAttention();
        foreach ($keys as $key) {
            if (isset($this->annotations[$key])) {
                $this->annotations[$key][] = $parser;
            } else {
                $this->annotations[$key] = [$parser];
            }
        }
        return $this;
    }

    /**
     * 设置输出结果的格式化对象
     * @param IDocParseResultFormater $docParseResultFormater 输出结果的格式化对象
     * @return AnnotationDocParse 当前对象本身
     */
    public function setOutputFormater(IDocParseResultFormater $docParseResultFormater): AnnotationDocParse
    {
        $this->outputFormater = $docParseResultFormater;
        return $this;
    }

    /**
     * 设置注解文档拆解的分隔器
     * @param IDocCommentIterator $it 注解文档拆解的分隔器
     * @return AnnotationDocParse 当前对象本身
     */
    public function setDocCommentIterator(IDocCommentIterator $it): AnnotationDocParse
    {
        $this->docIterator = $it;
        return $this;
    }

    /**
     * 解析注解文档
     * @param string $docComment 注解文档
     * @return array 解析结果，结构取决于对应格式化器
     */
    public function parse(string $docComment): array
    {
        $this->docIterator->setDocComment($docComment);
        $this->outputFormater->reset();
        foreach ($this->docIterator as $key => $val) {
            if (!isset($this->annotations[$key])) {
                continue;
            }
            foreach ($this->annotations[$key] as $annotation) {
                $this->outputFormater->addResult($annotation, $key, $annotation->parse($key, $val));
            }
        }
        return $this->outputFormater->getResult();
    }
}

# end of file
