<?php

namespace DreamCat\AnnotationParser\Factory;

use DreamCat\AnnotationParser\AnnotationDocParse;
use DreamCat\AnnotationParser\AnnotationFuncParser\FuncdescParser;
use DreamCat\AnnotationParser\AnnotationFuncParser\ParamParser;
use DreamCat\AnnotationParser\AnnotationFuncParser\ReturnParser;
use DreamCat\AnnotationParser\DocIterator\DefaultDocIterator;
use DreamCat\AnnotationParser\OutputFormater\FuncParseFormater;

/**
 * 函数注解解析器的工厂类
 * @author vijay
 */
class FuncParse
{
    /**
     * 函数注解解析器的工厂方法
     * @return AnnotationDocParse 解析器
     */
    public static function createFuncParse(): AnnotationDocParse
    {
        $parse = new AnnotationDocParse();
        $parse
            ->setDocCommentIterator(new DefaultDocIterator())
            ->setOutputFormater(new FuncParseFormater())
            ->addAnnotationParse(new FuncdescParser())
            ->addAnnotationParse(new ParamParser())
            ->addAnnotationParse(new ReturnParser());
        return $parse;
    }
}

# end of file
