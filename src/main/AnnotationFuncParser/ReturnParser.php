<?php

namespace DreamCat\AnnotationParser\AnnotationFuncParser;

use DreamCat\AnnotationParser\IAnnotationParse;

/**
 * 返回值解析器
 * @author vijay
 */
class ReturnParser implements IAnnotationParse
{
    /** @const string 未知类型的类型描述 */
    const TYPE_UNKNOWN = "unknown";
    /** @const string 空类型的类型描述，一般不包含返回值描述 */
    const TYPE_VOID = "void";

    /**
     * 获取此解析器关注的注解函数列表
     * @return string[] 关注的注解函数列表
     */
    public function getAttention(): array
    {
        return ["return"];
    }

    /**
     * 解析函数
     * @param string $key 注解函数
     * @param string $doc 注解函数后面的字符串
     * @return array 解析后的结果，结构如下{
     *      type : 返回值类型
     *      desc : 返回值描述
     * }
     */
    public function parse(string $key, string $doc)
    {
        if (trim($doc) == self::TYPE_VOID) {
            return [
                "type" => self::TYPE_VOID,
                "desc" => "",
            ];
        }
        $preg = "#^\\s*(?<type>[^\\s]+)(\\s+(?<desc>.+))$#";
        $doc = str_replace("\n", " ", $doc);
        if (preg_match($preg, $doc, $match)) {
            return [
                "type" => $match["type"],
                "desc" => $match["desc"] ?? "",
            ];
        } else {
            return [
                "type" => self::TYPE_UNKNOWN,
                "desc" => $doc,
            ];
        }
    }
}

# end of file
