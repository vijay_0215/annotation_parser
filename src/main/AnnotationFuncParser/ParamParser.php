<?php

namespace DreamCat\AnnotationParser\AnnotationFuncParser;

use DreamCat\AnnotationParser\IAnnotationParse;

/**
 * 参数解析器
 * @author vijay
 */
class ParamParser implements IAnnotationParse
{
    /** @const string 未知类型的类型描述 */
    const TYPE_UNKNOWN = "mixed";

    /**
     * 获取此解析器关注的注解函数列表
     * @return string[] 关注的注解函数列表
     */
    public function getAttention(): array
    {
        return ["param"];
    }

    /**
     * 解析函数
     * @param string $key 注解函数
     * @param string $doc 注解函数后面的字符串
     * @return mixed 解析后的结果，结构{
     *      var : 参数名
     *      type : 参数类型
     *      desc : 参数描述
     * }
     */
    public function parse(string $key, string $doc)
    {
        $doc = str_replace("\n", " ", $doc);
        if (preg_match("#^\\s*(?<type>[^\\s]+)\\s+\\$(?<var>\\w+)(?:\\s+(?<desc>.+))?$#", $doc, $match)) {
            return ["var" => $match["var"], "type" => $match["type"], "desc" => $match["desc"] ?? ""];
        } elseif (preg_match("#^\\s*\\$(?<var>\\w+)(?:\\s+(?<desc>.+))?$#", $doc, $match)) {
            return ["var" => $match["var"], "type" => self::TYPE_UNKNOWN, "desc" => $match["desc"] ?? ""];
        } else {
            return ["var" => "", "type" => self::TYPE_UNKNOWN, "desc" => $doc];
        }
    }
}

# end of file
