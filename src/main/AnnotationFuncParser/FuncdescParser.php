<?php

namespace DreamCat\AnnotationParser\AnnotationFuncParser;

use DreamCat\AnnotationParser\IAnnotationParse;

/**
 * 函数注解对未做@限制的解析
 * @author vijay
 * @note 这个类太简单的了，懒得写测试用例了
 */
class FuncdescParser implements IAnnotationParse
{
    /**
     * 获取此解析器关注的注解函数列表
     * @return string[] 关注的注解函数列表
     */
    public function getAttention(): array
    {
        return [""];
    }

    /**
     * 解析函数
     * @param string $key 注解函数
     * @param string $doc 注解函数后面的字符串
     * @return mixed 解析后的结果，由解析器决定结构
     */
    public function parse(string $key, string $doc)
    {
        return $doc;
    }
}

# end of file
