<?php

namespace DreamCat\AnnotationParser;

/**
 * 注解文档拆解的分隔器
 * @author vijay
 * @note 此接口负责将注解文档按各注解函数分拆开给特定文档函数的解析器解析
 */
interface IDocCommentIterator extends \Iterator
{
    /**
     * 设定注解文档
     * @param string $docComment 注解文档
     * @return IDocCommentIterator 当前对象自身
     */
    public function setDocComment(string $docComment): IDocCommentIterator;

    /** ====== 下面是迭代器函数，调用顺序 rewind -> valid -> 循环(current -> key -> next -> valid) ====== */
    /**
     * 返回当前注解函数的对应注解内容
     * @return string 当前注解函数的对应注解内容
     */
    public function current(): string;

    /**
     * 返回当前注解函数
     * @return string 当前注解函数
     */
    public function key(): string;

    /**
     * 向前移动到下一个元素
     * @return void
     */
    public function next(): void;

    /**
     * 返回到迭代器的第一个元素
     * @return void
     */
    public function rewind(): void;

    /**
     * 检查当前位置是否有效
     * @return bool 当前位置是否有效
     */
    public function valid(): bool;
}

# end of file
