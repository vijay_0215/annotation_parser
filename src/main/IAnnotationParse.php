<?php

namespace DreamCat\AnnotationParser;

/**
 * 特定文档函数的解析器
 * @author vijay
 * @note 所谓的关注的注解函数，是指注解文档中@后面到第一个空格前的字符串
 */
interface IAnnotationParse
{
    /**
     * 获取此解析器关注的注解函数列表
     * @return string[] 关注的注解函数列表
     */
    public function getAttention(): array;

    /**
     * 解析函数
     * @param string $key 注解函数
     * @param string $doc 注解函数后面的字符串
     * @return mixed 解析后的结果，由解析器决定结构
     */
    public function parse(string $key, string $doc);
}

# end of file
