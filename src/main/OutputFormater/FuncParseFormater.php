<?php

namespace DreamCat\AnnotationParser\OutputFormater;

use DreamCat\AnnotationParser\IAnnotationParse;
use DreamCat\AnnotationParser\IDocParseResultFormater;

/**
 * 函数解析
 * @author vijay
 */
class FuncParseFormater implements IDocParseResultFormater
{
    /** @var array 输出结果 */
    private $output;

    /**
     * 重置
     * @return void
     */
    public function reset(): void
    {
        $this->output = [
            "fnDesc" => [],
            "params" => [],
            "return" => [
                "type" => "void",
                "desc" => "",
            ],
        ];
    }

    /**
     * 添加输出结果
     * @param IAnnotationParse $annotationParse 解析器
     * @param string $key 注解函数
     * @param mixed $result 解析结果
     * @return void
     */
    public function addResult(IAnnotationParse $annotationParse, string $key, $result): void
    {
        switch ($key) {
            case "":
                $this->output["fnDesc"][] = $result;
                break;
            case "param":
                $this->output["params"][$result["var"]] = $result;
                break;
            case "return":
                $this->output["return"] = $result;
                break;
        }
    }

    /**
     * 获取输出结果
     * @return array 输出结果，结构{
     *      fnDesc :
     *      params :
     *      return :
     * }
     */
    public function getResult(): array
    {
        return $this->output;
    }
}

# end of file
