<?php

namespace DreamCat\AnnotationParser;

/**
 * 解析结果格式化接口
 * @author vijay
 */
interface IDocParseResultFormater
{
    /**
     * 重置
     * @return void
     */
    public function reset(): void;

    /**
     * 添加输出结果
     * @param IAnnotationParse $annotationParse 解析器
     * @param string $key 注解函数
     * @param mixed $result 解析结果
     * @return void
     */
    public function addResult(IAnnotationParse $annotationParse, string $key, $result): void;

    /**
     * 获取输出结果
     * @return array 输出结果，结构由实现类决定
     */
    public function getResult(): array;
}

# end of file
